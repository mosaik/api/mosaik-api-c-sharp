﻿using mosaik_api_c_sharp.API;
using mosaik_api_c_sharp.Configuration;

namespace IntegrationTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new Logger();
            try
            {
                //Read configuration
                XmlFileHandler configReader = new XmlFileHandler(Directory.GetCurrentDirectory() + "/resources/configuration.xml");
                args = configReader.GetArgs();//{mosaik-IP:mosaik-Port, API-Version, SimName, StepSize, boolean isServer}
                bool isSim = configReader.GetSimulationInfo(); //true = ExampleSim; false = ExampleMAS

                //Start the simulation
                if (isSim)
                {
                    ExampleSim sim = new ExampleSim(args);
                    SimProcess.startSimulation(args, sim);
                }
                else
                {
                    ExampleMAS sim = new ExampleMAS(args);
                    SimProcess.startSimulation(args, sim);
                }

                //Leave console open after finishing the program, if in Debug-Mode
                #if DEBUG
                Console.WriteLine("Press enter to close...");
                Console.ReadLine();
                #endif
            }
            catch (Exception e)
            {
                //Write exception to logger
                logger.Log("Error: " + e.Message);
                ConsoleWriter.WriteLine("Error: " + e.Message);
            }
        }
    }
}
