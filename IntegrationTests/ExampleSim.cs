﻿using mosaik_api_c_sharp.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IntegrationTests
{
    /// <summary>
    /// This class is an example how to handle the messages for the communication with mosaik.
    /// It serves also as an integration test.
    /// </summary>
    public class ExampleSim : Simulator
    {
        private long stepSize;
        private int idCounter = 0;
        private Dictionary<String, ExampleModel> instances;

        public ExampleSim(string[] args)
            : base()
        {
            this.simName = args[2];
            this.apiVersion = args[1];
            this.stepSize = long.Parse(args[3]);
            this.instances = new Dictionary<String, ExampleModel>();
        }

        public JObject CreateInitMessage(string apiVersion) 
        {
            return (JObject)JValue.Parse(("{"
                + "    'api_version': '" + apiVersion + "',"
                + "    'models': {"
                + "        'ExampleModel': {" + "            'public': true,"
                + "            'params': ['init_val'],"
                + "            'attrs': ['val', 'delta']" + "        }"
                + "    }" + "}").Replace("'", "\""));
        }

        #pragma warning disable
        public override JObject init(String sid, JObject simParams)
        {
            if (simParams["step_size"] != null)
            {
                this.stepSize = (int)simParams["step_size"];
            }
            return CreateInitMessage(this.apiVersion);
        }
        #pragma warning restore

        #pragma warning disable
        public override JArray create(int num, String model,
                JObject modelParams)
        {
            JArray entities = new JArray();
            for (int i = 0; i < num; i++)
            {
                String eid = "EM_" + (this.idCounter + i);
                if (modelParams["init_val"] != null)
                {
                    float initVal = ((float)modelParams["init_val"]);
                    this.instances.Add(eid, new ExampleModel(initVal));
                }
                else
                {
                    this.instances.Add(eid, new ExampleModel());
                }

                JObject entity = new JObject();
                entity.Add("eid", eid);
                entity.Add("type", model);
                entity.Add("rel", new JArray());
                entities.Add(entity);
            }
            this.idCounter += num;
            return entities;
        }
        #pragma warning restore

   
        public override void setupDone(){}

        #pragma warning disable
        public override long step(long time, JObject inputs)
        {
            foreach (JProperty entity in inputs.Children())
            {
                String eid = entity.Name;
                JObject attrs = (JObject)entity.Value;

                foreach (JProperty attr in attrs.Properties())
                {
                    String attrName = attr.Name;
                    
                    if (attrName.Equals("delta"))
                    {
                    }
                    else
                    {
                        continue;
                    }

                    // We don't care for the source eid and just use the values and
                    // sum them up.
                    Object[] values = attr.Values().ToArray();
                    float value = 0;
                    for (int i = 0; i < values.Length; i++)
                    {
                        value += (float)((JProperty)values[i]).Value;
                    }

                    // Get model instance and update it
                    ExampleModel instance = this.instances[eid];
                    instance.set_delta(value);
                }
            }

            foreach (ExampleModel instance in this.instances.Values)
            {
                instance.step();
            }

            return time + this.stepSize;
        }
        #pragma warning restore

        public override JObject getData(JObject outputs)
        {
            JObject data = new JObject();

            foreach (JProperty entity in outputs.Children())
            {
                String eid = entity.Name;
                JArray attrs = (JArray)entity.Value;
                JObject values = new JObject();
                ExampleModel instance = this.instances[eid];

                foreach (String attr in attrs) {
                    if (attr.Equals("val")) {
                	    values.Add(attr, instance.get_val());
                    }
                    else if (attr.Equals("delta")) {
                	    values.Add(attr, instance.get_delta());
                }
            }
                data.Add(eid, values);
            }
            return data;
        }

        public override void stop() 
        {
            Logger logger = new Logger();
            logger.Log("Message: stop message handling!");
        }

    }

    class ExampleModel
    {
        private float val;
        private float delta = 1; 

        public ExampleModel()
        {
            this.val = 0;
        }

        public ExampleModel(float initVal)
        {
            this.val = initVal;
        }

        public float get_val()
        {
            return this.val;
        }

        public float get_delta()
        {
            return this.delta;
        }

        public void set_delta(float delta)
        {
            this.delta = delta;
        }

        public void step()
        {
            this.val++;
        }
    }
}
