﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// Writes a message with a timestamp to the console
    /// </summary>
    public static class ConsoleWriter
    {
        public static void WriteLine(string message)
        {
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " - " + message);
        }
    }
}
