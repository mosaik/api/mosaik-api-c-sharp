﻿using System.Text;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// This class extends the API. It provides methods for handling the messages.
    /// </summary>
    public static class Extension
    {
        /// <summary>
        /// converts a string to a byte array. 
        /// </summary>
        /// <param name="messageString">string to convert into a byte array</param>
        /// <returns>the message as a byte array</returns>
        public static byte[] ToByteArray(this string messageString)
        {
            var asciiEncoding = new ASCIIEncoding();
            return asciiEncoding.GetBytes(messageString);
        }
    }
}
