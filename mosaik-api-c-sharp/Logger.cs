﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// This class creates a logging-file and writes lines to it.
    /// </summary>
    public class Logger
    {
        public string logFilePath;
        private System.IO.StreamWriter file;

        public Logger()
        {
            this.logFilePath = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\log.txt";
        }

        /// <summary>
        /// Write the string to a file.append mode is enabled so that the log
        /// lines get appended to  test.txt than wiping content and writing the log
        /// </summary>
        /// <param name="lines">String to write to the log-file</param>
        public void Log(String lines)
        {
            using (file = new System.IO.StreamWriter(logFilePath, true)) 
            {
                file.WriteLine(DateTime.Now.ToLongTimeString() + " - " + lines);
            }
        }


        /// <summary>
        /// Creates a new log file or overrides an existing one.
        /// </summary>
        public void CreateNewFile() 
        {
            using (file = new System.IO.StreamWriter(logFilePath, false))
            {
                file.WriteLine(DateTime.Now.ToLongTimeString() + " - Logging started!");
            }
        }
    }
}
