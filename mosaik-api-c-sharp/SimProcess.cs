﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// This class provides the network event loop and method dispatcher for your
    /// simulation.
    /// </summary>
    public class SimProcess
    {
        private static SimpyIoSocket sock;
        private static Simulator sim;
        private static bool stop;
        //private static Logger logger;
        public static Logger logger { get; set; }

        /// <summary>
        /// This method should be called from your main() in order
        /// to start the simulation.
        /// </summary>
        /// <param name="args">list of command line arguments</param>
        /// <param name="sim">the instance of your simulator class</param>
        #pragma warning disable
        public static void startSimulation(String[] args, Simulator sim)
        {
            String addr = args[0]; // e.g., "localhost:5555"

            // TODO: Read level from *args* 
            StartLogging(sim);
            sim.configure(args);

            try {
                sock = new SimpyIoSocket(addr);
                stop = false;
                sim.mosaik = new MosaikProxy(sock);
                Run(sim);
            } catch (Exception) {
                throw;
            } finally {
                sim.cleanup();
            }
        }

        /// <summary>
        /// Start logging: create a new file and write the first line
        /// </summary>
        /// <param name="sim">Instance of Simulator</param>
        private static void StartLogging(Simulator sim)
        {
            logger = new Logger();
            logger.CreateNewFile();
            logger.Log("Starting " + sim.simName + " ...");
        }
        #pragma warning restore

        /**
         * @param addr is mosaik's network address <em>host:port</em>.
         * @param simulator is the simulator instance.
         * @throws Exception
         */
        //private SimProcess(String addr, Simulator simulator){
        //    sock = new SimpyIoSocket(addr);
        //    sim = simulator;
        //    stop = false;

        //    sim.mosaik = new MosaikProxy(sock);
        //}

        /// <summary>
        /// This method implements the event-loop.
        /// It dispatches method call requests until it receives a stop message.
        /// </summary>
        /// <param name="sim">Instance of Simulator</param>
        #pragma warning disable
        public static void Run(Simulator sim)
        {
            try
            {
                ConsoleWriter.WriteLine("Start Simulation!");
                mosaik_api_c_sharp.API.SimpyIoSocket.Request req;
                JObject result = new JObject();
                JArray resultArray = new JArray();

                while (!stop)
                {
                    req = sock.recvRequest();
                    bool arrayReply = false;
                    long resultNumber = 0;
                    bool numberReply = false;
                    switch (req.method)
                    {
                        case "init":
                            String sid = (String)req.args[0];
                            result = sim.init(sid, req.kwargs);
                            logger.Log("mosaik message - init: " + result);
                            break;

                        case "create":
                            int num = int.Parse((string)(req.args[0]));
                            String model = (String)req.args[1];
                            resultArray = sim.create(num, model, req.kwargs);
                            arrayReply = true;
                            logger.Log("mosaik message - create: " + resultArray);
                            break;

                        case "setup_done":
                            result = null;
                            sim.setupDone();
                            logger.Log("mosaik message - setupdone");
                            break;

                        case "step":
                            long time = ((long)req.args[0]);
                            JObject inputs = (JObject)req.args[1];
                            resultNumber = sim.step(time, inputs);
                            numberReply = true;
                            logger.Log("mosaik message - step: " + inputs);
                            break;

                        case "get_data":
                            JObject outputs = (JObject)req.args[0];
                            result = sim.getData(outputs);
                            logger.Log("mosaik message - get_data: " + outputs);
                            break;

                        case "stop":
                            logger.Log("mosaik message - stop");
                            sim.stop();
                            stop = true;
                            break;

                        default:
                            throw new Exception("Unkown method: " + req.method);
                    }

                    if (stop)
                        break;

                    Reply(req, result, resultArray, ref arrayReply, resultNumber, ref numberReply);
                }
                sock.Close();
                logger.Log(sim.simName + " finished");
                ConsoleWriter.WriteLine("Stop Simulation!");
            }
            catch (Exception)
            {
                throw;
            }
        }
        #pragma warning restore

        /// <summary>
        /// Replies with the specific result type.
        /// </summary>
        /// <param name="req">Instance of Request</param>
        /// <param name="result">the result as a JObject</param>
        /// <param name="resultArray">the result as an JArray</param>
        /// <param name="arrayReply">boolean if it's an array or not</param>
        /// <param name="resultNumber">boolean if it's a numeric result or not</param>
        /// <param name="numberReply">the result as a numeric value</param>
        private static void Reply(mosaik_api_c_sharp.API.SimpyIoSocket.Request req, JObject result, JArray resultArray, ref bool arrayReply, long resultNumber, ref bool numberReply)
        {
            if (numberReply)
            {
                req.reply(resultNumber);
                numberReply = false;
            }
            else if (arrayReply)
            {
                req.reply(resultArray);
                arrayReply = false;
            }
            else
            {
                req.reply(result);
            }
        }
        
    }
}
