﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace mosaik_api_c_sharp.Configuration
{
    /// <summary>
    /// This class provides a XML parser and reads the configuration.xml to get the needed
    /// information to connect to mosaik and run the simulation
    /// </summary>
    public class XmlFileHandler : IDisposable
    {
        private XmlDocument xmldoc;
        private Stream stream;
        private string path;

        public XmlFileHandler()
        {

        }

        public XmlFileHandler(string path)
        {
            this.path = path;
        }

        /// <summary>
        /// Start the FileStream
        /// </summary>
        private void StartFileStream()
        {
            if (this.path == null)
            {
                this.stream = this.GetType().Assembly.GetManifestResourceStream("mosaik_api_c_sharp.resources.configuration.xml");
            }
            else
            {
                this.stream = new FileStream(this.path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);

            }
            this.xmldoc = new XmlDocument();
            this.xmldoc.Load(stream);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                this.stream.Close();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Get information about the simulation by a key
        /// </summary>
        /// <param name="key">integer which is using as a key to get the specific information</param>
        /// <returns>the specific information</returns>
        private string ReadSimInfo(int key)
        {
            if (key == 0)
                return this.xmldoc.SelectSingleNode("Settings/SimInfo/Name").InnerText;
            else if (key == 1)
                return this.xmldoc.SelectSingleNode("Settings/SimInfo/isServer").InnerText;
            else
                return this.xmldoc.SelectSingleNode("Settings/SimInfo/isSim").InnerText;
        }

        /// <summary>
        /// Get the server settings by a key
        /// </summary>
        /// <param name="key">integer which is using as a key to get the specific information</param>
        /// <returns>the specific information</returns>
        private string ReadServerConfig(int key)
        {
            if (key == 0)
                return this.xmldoc.SelectSingleNode("Settings/SimServer/IP-Address").InnerText;
            else if (key == 1)
                return this.xmldoc.SelectSingleNode("Settings/SimServer/Port").InnerText;
            else
                return this.xmldoc.SelectSingleNode("Settings/SimServer/StepSize").InnerText;
        }

        /// <summary>
        /// Writes the server settings
        /// </summary>
        /// <param name="ip">IP-Address for the server</param>
        /// <param name="port">Port number for the server</param>
        public void WriteServerConfig(string ip, string port)
        {
            this.xmldoc.SelectSingleNode("Settings/SimServer/IP-Address").InnerText = ip;
            this.xmldoc.SelectSingleNode("Settings/SimServer/Port").InnerText = port;
            this.xmldoc.Save(stream);
        }

        /// <summary>
        /// Get the mosaik setting by a key
        /// </summary>
        /// <param name="key">integer which is using as a key to get the specific information</param>
        /// <returns>the specific information</returns>
        private string ReadMosaikConfig(int key)
        {
            if (key == 0)
                return this.xmldoc.SelectSingleNode("Settings/Mosaik/IP-Address").InnerText;
            else if (key == 1)
                return this.xmldoc.SelectSingleNode("Settings/Mosaik/Port").InnerText;
            else
                return this.xmldoc.SelectSingleNode("Settings/Mosaik/API-Version").InnerText;
        }

        /// <summary>
        /// 
        /// writes the mosaik settings
        /// </summary>
        /// <param name="ip">ip-Address for mosaik</param>
        /// <param name="port">port number for mosaik</param>
        /// <param name="apiVersion">API version number from mosaik</param>
        public void WriteMosaikConfig(string ip, string port, string apiVersion)
        {
            this.xmldoc.SelectSingleNode("Settings/Mosaik/IP-Address").InnerText = ip;
            this.xmldoc.SelectSingleNode("Settings/Mosaik/Port").InnerText = port;
            this.xmldoc.SelectSingleNode("Settings/Mosaik/API-Version").InnerText = apiVersion;
            this.xmldoc.Save(stream);
        }

        /// <summary>
        /// Gets the connection settings from the configuration file
        /// </summary>
        public string[] GetConnectionSettings()
        {
            string[] connInfo = new string[7];
            StartFileStream();
            connInfo[0] = ReadMosaikConfig(0); //serverIp
            connInfo[1] = ReadMosaikConfig(1); //serverPort
            connInfo[2] = ReadMosaikConfig(2); //API version number
            connInfo[3] = ReadSimInfo(2); //SimName
            connInfo[4] = ReadServerConfig(2); //StepSize
            connInfo[5] = ReadServerConfig(0); //Listener IP
            connInfo[6] = ReadServerConfig(1); //Listener Port
            Close();

            return connInfo;
        }

        /// <summary>
        /// Gets the connection settings from the configuration file
        /// </summary>
        public string[] GetArgs()
        {
            string[] connInfo = new string[5];
            StartFileStream();
            string isServer = ReadSimInfo(1);
            if (isServer.Equals("true"))
                connInfo[0] = ReadServerConfig(0) + ":" + ReadServerConfig(1);
            else
                connInfo[0] = ReadMosaikConfig(0) + ":" + ReadMosaikConfig(1); //serverIp:serverPort
            connInfo[1] = ReadMosaikConfig(2); //API version number
            connInfo[2] = ReadSimInfo(0); //SimName
            connInfo[3] = ReadServerConfig(2); //StepSize
            connInfo[4] = isServer; //boolean isServer
            Close();

            return connInfo;
        }

        /// <summary>
        /// Gets the informations if it's a simulation or MAS (true = simulation; false = MAS).
        /// </summary>
        /// <returns>boolean if it's a simulation</returns>
        public bool GetSimulationInfo()
        {
            StartFileStream();
            bool isSim = bool.Parse(ReadSimInfo(2));
            Close();
            return isSim;
        }

        /// <summary>
        /// Close the reader
        /// </summary>
        public void Close()
        {
            stream.Flush();
            Dispose();
            stream.Close();
        }
    }
}
