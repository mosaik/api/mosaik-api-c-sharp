﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// This class provides the message handling methods
    /// </summary>
    public abstract class Simulator
    {
        public string simName { get; set;}
        public string apiVersion { get; set; }
        public MosaikProxy mosaik { get; set; }

        /// <summary>
        /// Create a new simulator instance.
        /// </summary>
        public Simulator()
        {
        }

        /// <summary>
        /// Initialize the simulator with the ID <em>sid</em> and apply additional
        /// parameters <em>(simParams)</em> sent by mosaik.
        /// </summary>
        /// <param name="sid">the ID mosaik has given to this simulator</param>
        /// <param name="simParams">a map with additional simulation parameters</param>
        /// <returns>the meta data dictionary (see {@link
        ///     https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#init})    
        /// </returns>
        public abstract JObject init(String sid, JObject simParams);

        /// <summary>
        /// Create <em>num</em> instances of <em>model</em> using the provided
        /// <em>model_params</em>.
        /// </summary>
        /// <param name="num"> the number of instances to create</param>
        /// <param name="model">the name of the model to instantiate. It needs to be
        /// listed in the simulator's meta data and be public</param>
        /// <param name="modelParams">a map containing additional model parameters</param>
        /// <returns>a (nested) list of maps describing the created entities (model
        /// instances) (see {@link
        ///     https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#create})
        ///     </returns>
        public abstract JArray create(int num, String model,
                JObject modelParams);

        /// <summary>
        /// Callback that indicates that the scenario setup is done and the actual simulation is about to start. 
        /// At this point, all entities and all connections between them are know but no simulator has been stepped yet.
        /// Implementing this method is optional.
        /// Added in mosaik API version 3.
        /// </summary>
        public virtual void setupDone() { }

        /// <summary>
        /// Perform the next simulation step from time <em>time</em> using input
        /// values from <em>inputs</em> and return the new simulation time (the time
        /// at which <code>step()</code> should be called again).
        /// </summary>
        /// <param name="time">the current time in seconds from simulation start</param>
        /// <param name="inputs">a map of input values (see {@link
        ///     https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#step})
        ///     </param>
        /// <returns>the time at which this method should be called again in seconds
        /// since simulation start</returns>
        public abstract long step(long time, JObject inputs);

        /// <summary>
        /// Return the data for the requested attributes in 'outputs'
        /// </summary>
        /// <param name="outputs"> a mapping of entity IDs to lists of attribute names</param>
        /// <returns>a mapping of the same entity IDs to maps with attributes and
        /// their values (see {@link
        ///     https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#get-data})
        /// </returns>
        public abstract JObject getData(JObject outputs);

        /// <summary>
        /// Will be called after the simulation finished
        /// </summary>
        /// (see {@link
        ///     https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#stop})
        public abstract void stop();

        /// <summary>
        /// This method can be overridden to configure the simulation with the
        /// command line 'args'.
        /// The default implementation simply ignores them.
        /// </summary>
        /// <param name="args">a list of command line arguments</param>
        public void configure(String[] args)
        {
            if (args.Length > 4)
            {
                if (args[4].ToLower().Equals("true"))
                {
                    SimpyIoSocket.isServer = true;
                }
            }

            return;
        }

        /// <summary>
        /// This method is executed just before the sim process stops.
        /// Use this to perform some clean-up, e.g., to terminate external processes
        /// that you started or to join threads.
        /// </summary>
        public void cleanup()
        {
            return;
        }
    }
}
