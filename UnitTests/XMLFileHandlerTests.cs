using mosaik_api_c_sharp.Configuration;

namespace UnitTests
{
    [TestClass]
    public class XMLFileHandlerTests
    {
        [TestMethod]
        public void IsValidArgumentsArray()
        {
            XmlFileHandler xmlFileHandler = new XmlFileHandler();
            string[] result = xmlFileHandler.GetArgs();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }

        [TestMethod]
        public void IsValidIsSimulationValue()
        {
            XmlFileHandler xmlFileHandler = new XmlFileHandler();
            bool isSimulation = xmlFileHandler.GetSimulationInfo();
            Assert.IsNotNull(isSimulation);
        }

        [TestMethod]
        public void IsValidConnectionSettingsArray()
        {
            XmlFileHandler xmlFileHandler = new XmlFileHandler();
            string[] result = xmlFileHandler.GetConnectionSettings();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }
    }
}