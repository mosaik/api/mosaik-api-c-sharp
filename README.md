## Mosaik API for C#
This is an implementation of the mosaik API for simulators written in C#.

It hides all the messaging and networking related stuff and provides a simple
base class `Simulator` that you can implement.

## Setup
Clone or download this repository onto you computer. Use Nuget to install the Newtonsoft.JSON and other packages.
You can open the project in Visual Studio Code. Be sure to have .NET installed on your system.
  
### Build the solution
Use `dotnet build` to build the project.

## Testing
### Run the unit tests
You can run the unit tests with `dotnet test UnitTests/UnitTests.csproj`.

### Run the integration tests with python

#### 1. Prepare Python for mosaik
To run the test cases, you will need Python 3 and
the virtualenv package for your current Python environment installed.

Create a new Python 3 virtualenv an install all required packages with `pip install -r python-tests/requirements.txt`.
You can let Pytest run your tests with `pytest python-tests/test_examplesim.py`. Nevertheless, for them to run sucessfully, 
you need to start the C# integration test accordingly.

You can optionally pass the parameter -s to activate Pytests console output 
for additional debug information.

#### 2. Run the integration test
First start the virtualenv for mosaik and run the desired test (for instance `pytest test_examplesim.py`).

In VSCode in the project explorer check the `IntegrationTests/resources/configuration.xml` file. If you have started 
the `test_examplesim.py` file make sure the `<isSim>` tag is set to true and the `<isServer>` tag is set to false.
 
In the file `IntegrationTests/Program.cs` click on "Run" or direct into the `IntegrationTests` directory and type `dotnet run`,
like to: `~/develop/mosaik-api-c-sharp/IntegrationTests$ dotnet run`.

You should now see output both on the Python and the C# side.

### Logging
To use the logger you have to initiate the logger in your class. 
Here is a simple example how to use the logger:

```C#
Logger logger = new Logger();
logger.Log("Message: Trying to start client"); 
```

The logger will write the logs into your application folder.

## Documentation
You can find general information about the API in mosaik's docs
 (https://mosaik.readthedocs.org/en/latest/mosaik-api/). 
Also, all public classes and methods also have xmldocstrings .

The *mosaik-api-c-sharp-integration-testing/* directory contains an example simulator (ExampleSim) and an
example control strategy (ExampleMAS) that may give you an idea of what to do
and how things work.

### Support
If you need help or want to give feedback, you are welcome to post something
to our mailing list (https://mosaik.offis.de/mailinglist). 
You can also browse the archives (https://lists.offis.de/pipermail/mosaik-users/).